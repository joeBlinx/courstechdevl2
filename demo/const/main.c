#include <stddef.h>
void const_right() {
  int const a = 5;
  // a = 6;
  const float  b = 54.0;
  // b = 7;
  double const c = 3.14;
  // c = 34.;
  char h = 'h';
  char* const ptr = &h;
  *ptr = 'd';
  // ptr = NULL;
}
void const_left() {
  const int a = 5;
  // a = 6;
  const float b = 54.0;
  // b = 7;
  const double c = 3.14;
  // c  = 34.;
  const char h = 'h';

  char const r = 'r';

  char * const ptr = &h;
  //*ptr = 'd';
  //ptr = NULL;
}
int main(void) {
  int a = 0;
  const int* ptr = &a;
  ptr = NULL;
  int const* ptr2 = &a;
  int* const ptr3 = &a;
  
  int const* const ptr4 = &a;
  const int* const ptr5 = &a;
}
