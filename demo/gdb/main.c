
#include <stdio.h>
#include <stdlib.h>

void check(int number, int guessed_number){
    if(number < guessed_number){
        puts("Trop petit");
    }else if(number > guessed_number){
        puts("Trop grand");
    }
}

int main(){
    int const number = rand()%256;
    int guessed_number = 0;
    do{
        puts("Entrer un nombre");
        scanf("%d", &guessed_number);
        check(number, guessed_number);

    }while(guessed_number != number);

    puts("C'est gagné!");
    
}