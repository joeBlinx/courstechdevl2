#include <stdio.h>
void add_forty_two(int*, int);


void display(int* array, int index){
	printf("array[%d] = %d\n", index, *(array + index));
}

void divide_2(int* array, int index){
	*(array + index) /= 2;
}
void for_each(int* array, int size, void(*function)(int*, int)){
	for(int index = 0; index < size; index += 1){
		function(array, index);
	}
}
int main(void){
	void(*fct_ptr)(int*, int, void(*)(int*, int)) = for_each;

	int array[] = {5, 6, 2, 45, 565, 65};
	int size = sizeof array/sizeof *array;
	/*for(int i = 0; i < size; i += 1){
		add_forty_two(array, i);
	}*/
	for_each(array, size, display);
	for_each(array, size, add_forty_two);
	/*for(int i = 0; i < size; i += 1){
		display(array, i);
	}*/
	for_each(array, size, display);
	for_each(array, size, divide_2);
	for_each(array, size, display);
}
void add_forty_two(int* array, int index){
	*(array + index) += 42;
	
}