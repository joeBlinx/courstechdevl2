#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include "callback.h"
#include <stdbool.h>
#include "program.h"
#include "vao.h"
#include "init.h"
#include "buffer.h"
#include "display.h"

int main(void){

    if(!glfwInit()){
        fprintf(stderr, "Gflw not initialized");
        return 1;
    }
    glfwSetErrorCallback(error_init);
    
    GLFWwindow* window = glfwCreateWindow(width, height, "square", NULL, NULL);
    if(!window){
        goto end;
    }
    glfwMakeContextCurrent(window);
    
    glfwSetKeyCallback(window, key_callback);

    if(init_gl()){
        goto destroy_window;
    }
    
    buffer_pool_t buffer_pool;
    init_buffer_pool(&buffer_pool, 20);

    GLint program = create_program("src/shader/vert.glsl", "src/shader/frag.glsl");

    indirect_buffer_data_t indirect_render = {4, 1, 0, 0};
    square_data_t square = {
        .vao = init_vao(),
        .vbo = next_buffer(&buffer_pool),
        .indirect_buffer = next_buffer(&buffer_pool),
        .program = program,
        .uniform = glGetUniformLocation(program, "angle")
    };

    init_vbo(square.vao, square.vbo, vertices, sizeof vertices);

    glNamedBufferData(square.indirect_buffer, sizeof indirect_render, &indirect_render, GL_STATIC_DRAW);
    
    float angle = 0.0f;
    while(!glfwWindowShouldClose(window)){
        glfwPollEvents();
        float clear_color[]={
            0.5, 0.5, 0.5, 1
        };
        
        glClearBufferfv(GL_COLOR, 0, clear_color);
        angle += 0.01;
        display(&square, angle);
        glfwSwapBuffers(window);
    }
    

    glDeleteProgram(program);
    glDeleteVertexArrays(1, &square.vao);
    release_pool(&buffer_pool);
    destroy_window:
    glfwDestroyWindow(window);
    end:
    glfwTerminate(); 
}