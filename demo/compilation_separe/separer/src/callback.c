#include <stdio.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
void error_init(int error, const char* description)
{
    (void)error;
    fprintf(stderr, "Error: %s\n", description);
}


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    (void)mods;
    (void)scancode;
    (void)action;
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS){
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
}

void gl_debug_callback(GLenum source, GLenum type, GLuint stuff,
                                  GLenum severity, GLsizei size, const GLchar* message, const void* data){
    (void)source;
    (void)type;
    (void)severity;
    (void)size;
    (void)data;
    (void)stuff;
    fprintf(stderr, "Error message is: %s\n", message);
}

