#pragma once
#include <GL/gl.h>

GLuint init_vao(void);
void init_vbo(GLuint vao, GLuint vbo, const float data[], size_t size);