#pragma once
#include <GL/gl.h>
#include <stddef.h>

struct indirect_buffer_data
{
    GLuint count;
    GLuint instanceCount;
    GLuint first;
    GLuint baseInstance;
};
typedef struct indirect_buffer_data indirect_buffer_data_t;

struct buffer_pool{
    GLuint* buffers;
    size_t size;
    uint32_t next;
};

typedef struct buffer_pool buffer_pool_t;

void init_buffer_pool(buffer_pool_t* pool, size_t size);
GLuint next_buffer(buffer_pool_t* pool);
void release_pool(buffer_pool_t* pool);
