#include <GL/glew.h>
#include <GL/gl.h>

GLuint init_vao(void){
    GLuint vao = 0;
    glGenVertexArrays(1, &vao);
    return vao;
}

void init_vbo(GLuint vao, GLuint vbo, float data[], size_t size){

    glBindVertexArray(vao);
    glNamedBufferData(vbo, size, data, GL_STATIC_DRAW);
    glVertexArrayVertexBuffer(vao, 0, vbo, 0, 2*sizeof(float));
    glEnableVertexArrayAttrib(vao, 0);
    glVertexArrayAttribBinding(vao, 0, 0);
    glVertexArrayAttribFormat(vao, 0, 2, GL_FLOAT, GL_FALSE, 0);

}