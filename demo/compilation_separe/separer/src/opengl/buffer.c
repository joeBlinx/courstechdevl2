#include <GL/glew.h>
#include "buffer.h"
#include <stdlib.h>

void init_buffer_pool(buffer_pool_t* pool, size_t size){
    pool->buffers = malloc(size*sizeof *pool->buffers);
    pool->size = size;
    pool->next = 0;
    glCreateBuffers(size, pool->buffers);
}

GLuint next_buffer(buffer_pool_t* pool){
    return pool->buffers[pool->next++];
}

void release_pool(buffer_pool_t* pool){
    glDeleteBuffers(pool->size, pool->buffers);
    free(pool->buffers);
}