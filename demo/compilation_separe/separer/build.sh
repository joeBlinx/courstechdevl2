#!/bin/bash
function display_and_run(){
    echo "$1"
    $1
}
set -e
sources_files="callback.c
            main.c
            display.c
            vao.c
            program.c
            buffer.c"


CFLAGS="-Wall -pedantic-errors -std=c11 -I$PWD/src/opengl"
LDFLAGS="-lGL -lGLEW -lglfw"
object_file=
mkdir -p build
for file in $sources_files
do
    file_to_build=$(find -name "${file}")
    display_and_run "gcc ${CFLAGS} -c ${file_to_build} -o build/${file}.o"
    object_file+="build/${file}.o "
done

display_and_run "gcc ${object_file} -o build/exe ${LDFLAGS}"


