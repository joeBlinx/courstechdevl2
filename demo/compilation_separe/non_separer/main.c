#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

struct indirect_buffer_data
{
    GLuint count;
    GLuint instanceCount;
    GLuint first;
    GLuint baseInstance;
};
typedef struct indirect_buffer_data indirect_buffer_data_t;

struct buffer_pool{
    GLuint* buffers;
    size_t size;
    uint32_t next;
};

typedef struct buffer_pool buffer_pool_t;
void init_buffer_pool(buffer_pool_t* pool, size_t size){
    pool->buffers = malloc(size*sizeof *pool->buffers);
    pool->size = size;
    pool->next = 0;
    glCreateBuffers(size, pool->buffers);
}

GLuint next_buffer(buffer_pool_t* pool){
    return pool->buffers[pool->next++];
}

void release_pool(buffer_pool_t* pool){
    glDeleteBuffers(pool->size, pool->buffers);
    free(pool->buffers);
}
static char* extract_data_from_file(const char* file_path){
    FILE* file = fopen(file_path, "r");
    if(!file){
        fprintf(stderr, "file: %s not found", file_path);
        return NULL;
    }
    char* data = NULL;
    int number_alloc = 0;
    while(!feof(file)){
        number_alloc += 1;
        int const size_chunk = 500;
        int const size = size_chunk*number_alloc;
        data = realloc(data, size);
        char* current_data = data + (size_chunk * (number_alloc-1));
        
        memset(current_data, 0, size_chunk);
        fread(current_data, 1, size_chunk, file);
    }
    fclose(file);
    return data;
}

static GLuint build_one_shader(const char* file, GLenum shader_type){
    GLint result = GL_FALSE;
    GLuint shaderID = glCreateShader(shader_type);
    char* data = extract_data_from_file(file);
    glShaderSource(shaderID, 1, (const char**)&data, NULL);
    glCompileShader(shaderID);
    int infoLog = 0;
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
    glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLog);

    if (infoLog > 0) {
        char error[infoLog];
        memset(error, 0, sizeof(error));
        glGetShaderInfoLog(shaderID, infoLog, NULL, error);
        printf("Shader data is: %s\n error is: %s", data, error);

    }
    free(data);
    return shaderID;
}

GLint create_program(const char* vertex, const char* fragment){
    GLuint vertex_shader = build_one_shader(vertex, GL_VERTEX_SHADER);
    GLuint fragment_shader = build_one_shader(fragment, GL_FRAGMENT_SHADER);

    GLint program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);

    glLinkProgram(program);

    glDetachShader(program, vertex_shader);
    glDetachShader(program, fragment_shader);

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
    return program;
}

GLuint init_vao(void){
    GLuint vao = 0;
    glGenVertexArrays(1, &vao);
    return vao;
}

void init_vbo(GLuint vao, GLuint vbo, float const data[], size_t size){

    glBindVertexArray(vao);
    glNamedBufferData(vbo, size, data, GL_STATIC_DRAW);
    glVertexArrayVertexBuffer(vao, 0, vbo, 0, 2*sizeof(float));
    glEnableVertexArrayAttrib(vao, 0);
    glVertexArrayAttribBinding(vao, 0, 0);
    glVertexArrayAttribFormat(vao, 0, 2, GL_FLOAT, GL_FALSE, 0);

}
void error_init(int error, const char* description)
{
    (void)error;
    fprintf(stderr, "Error: %s\n", description);
}


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    (void)mods;
    (void)scancode;
    (void)action;
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS){
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
}

void gl_debug_callback(GLenum source, GLenum type, GLuint stuff,
                                  GLenum severity, GLsizei size, const GLchar* message, const void* data){
    (void)source;
    (void)type;
    (void)severity;
    (void)size;
    (void)data;
    (void)stuff;
    fprintf(stderr, "Error message is: %s\n", message);
}


struct square_data
{
    GLuint vao;
    GLuint vbo;
    GLuint indirect_buffer;
    GLuint program;
    GLint uniform;
};

typedef struct square_data square_data_t;
void display(square_data_t const *data, float const angle){

    glBindBuffer(GL_DRAW_INDIRECT_BUFFER, data->indirect_buffer);
    glUseProgram(data->program);
    glBindVertexArray(data->vao);
    glUniform1f(data->uniform, angle);
    glDrawArraysIndirect(GL_TRIANGLE_STRIP, 0);

}

void display(square_data_t const *data, float angle);

struct GLFWwindow;
void error_init(int error, const char* description);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void gl_debug_callback(GLenum source, GLenum type, GLuint stuff,
                                  GLenum severity, GLsizei size, const GLchar* message, const void* data);
static int const width = 1276;
static int const height = 720;
static const float vertices[8] = {
                    -0.5, 0.5,
                    0.5, 0.5,
                    -0.5, -0.5,
                    0.5, -0.5
                };

static void init_debug(void){
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, NULL, GL_FALSE);
    glDebugMessageCallback(gl_debug_callback, NULL);
}

static int init_gl(){
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK){
        fprintf(stderr, "Error while initialize OpenGL");
        return -1;
    }
    init_debug();
    return 0;
}
int main(void){

    if(!glfwInit()){
        fprintf(stderr, "Gflw not initialized");
        return 1;
    }
    glfwSetErrorCallback(error_init);
    
    GLFWwindow* window = glfwCreateWindow(width, height, "square", NULL, NULL);
    if(!window){
        goto end;
    }
    glfwMakeContextCurrent(window);
    
    glfwSetKeyCallback(window, key_callback);

    if(init_gl()){
        goto destroy_window;
    }
    
    buffer_pool_t buffer_pool;
    init_buffer_pool(&buffer_pool, 20);

    GLint program = create_program("src/shader/vert.glsl", "src/shader/frag.glsl");

    indirect_buffer_data_t indirect_render = {4, 1, 0, 0};
    square_data_t square = {
        .vao = init_vao(),
        .vbo = next_buffer(&buffer_pool),
        .indirect_buffer = next_buffer(&buffer_pool),
        .program = program,
        .uniform = glGetUniformLocation(program, "angle")
    };

    init_vbo(square.vao, square.vbo, vertices, sizeof vertices);

    glNamedBufferData(square.indirect_buffer, sizeof indirect_render, &indirect_render, GL_STATIC_DRAW);
    
    float angle = 0.0f;
    while(!glfwWindowShouldClose(window)){
        glfwPollEvents();
        float clear_color[]={
            0.5, 0.5, 0.5, 1
        };
        
        glClearBufferfv(GL_COLOR, 0, clear_color);
        angle += 0.01;
        display(&square, angle);
        glfwSwapBuffers(window);
    }
    

    glDeleteProgram(program);
    glDeleteVertexArrays(1, &square.vao);
    release_pool(&buffer_pool);
    destroy_window:
    glfwDestroyWindow(window);
    end:
    glfwTerminate(); 
}