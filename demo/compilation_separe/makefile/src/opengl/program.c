#include <GL/glew.h>
#include <GL/gl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char* extract_data_from_file(const char* file_path){
    FILE* file = fopen(file_path, "r");
    if(!file){
        fprintf(stderr, "file: %s not found", file_path);
        return NULL;
    }
    char* data = NULL;
    int number_alloc = 0;
    while(!feof(file)){
        number_alloc += 1;
        int const size_chunk = 500;
        int const size = size_chunk*number_alloc;
        data = realloc(data, size);
        char* current_data = data + (size_chunk * (number_alloc-1));
        
        memset(current_data, 0, size_chunk);
        fread(current_data, 1, size_chunk, file);
    }
    fclose(file);
    return data;
}

static GLuint build_one_shader(const char* file, GLenum shader_type){
    GLint result = GL_FALSE;
    GLuint shaderID = glCreateShader(shader_type);
    char* data = extract_data_from_file(file);
    glShaderSource(shaderID, 1, (const char**)&data, NULL);
    glCompileShader(shaderID);
    int infoLog = 0;
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
    glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLog);

    if (infoLog > 0) {
        char error[infoLog];
        memset(error, 0, sizeof(error));
        glGetShaderInfoLog(shaderID, infoLog, NULL, error);
        printf("Shader data is: %s\n error is: %s", data, error);

    }
    free(data);
    return shaderID;
}

GLint create_program(const char* vertex, const char* fragment){
    GLuint vertex_shader = build_one_shader(vertex, GL_VERTEX_SHADER);
    GLuint fragment_shader = build_one_shader(fragment, GL_FRAGMENT_SHADER);

    GLint program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);

    glLinkProgram(program);

    glDetachShader(program, vertex_shader);
    glDetachShader(program, fragment_shader);

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
    return program;
}