#pragma once
#include <GL/gl.h>

GLint create_program(const char* vertex, const char* fragment);