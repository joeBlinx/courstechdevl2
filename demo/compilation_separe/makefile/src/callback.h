#pragma once
#include <GL/gl.h>

struct GLFWwindow;
void error_init(int error, const char* description);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void gl_debug_callback(GLenum source, GLenum type, GLuint stuff,
                                  GLenum severity, GLsizei size, const GLchar* message, const void* data);