#pragma once
#include <stdio.h>
#include <GL/glew.h>
#include "callback.h"
static int const width = 1276;
static int const height = 720;
static const float vertices[8] = {
                    -0.5, 0.5,
                    0.5, 0.5,
                    -0.5, -0.5,
                    0.5, -0.5
                };

static void init_debug(void){
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, NULL, GL_FALSE);
    glDebugMessageCallback(gl_debug_callback, NULL);
}

static int init_gl(){
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK){
        fprintf(stderr, "Error while initialize OpenGL");
        return -1;
    }
    init_debug();
    return 0;
}