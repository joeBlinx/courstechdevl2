#include <GL/glew.h>
#include "display.h"

void display(square_data_t const *data, float const angle){

    glBindBuffer(GL_DRAW_INDIRECT_BUFFER, data->indirect_buffer);
    glUseProgram(data->program);
    glBindVertexArray(data->vao);
    glUniform1f(data->uniform, angle);
    glDrawArraysIndirect(GL_TRIANGLE_STRIP, 0);

}