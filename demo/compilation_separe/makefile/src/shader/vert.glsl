#version 400

layout(location = 0) in vec2 pos;
uniform float angle;

void main(){
    float width = 1276.;
    float height = 720.;
    mat3 rotate_matrix = mat3(
        cos(angle), -sin(angle), 0,
        sin(angle), cos(angle), 0,
        0, 0, 1
    );
    mat3 ratio = mat3(
        height/width, 0, 0,
        0, 1, 0,
        0, 0, 1
    );
    vec3 new_pos = ratio*rotate_matrix*vec3(pos, 0);
    gl_Position = vec4(new_pos, 1);
}