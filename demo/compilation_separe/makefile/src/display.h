#pragma once
#include <GL/gl.h>

struct square_data
{
    GLuint vao;
    GLuint vbo;
    GLuint indirect_buffer;
    GLuint program;
    GLint uniform;
};

typedef struct square_data square_data_t;

void display(square_data_t const *data, float angle);