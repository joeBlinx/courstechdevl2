#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]){
    if(argc > 1){
        int const number = atoi(argv[1]);
        printf("The first argument of program %s is %d", argv[0], number);
    }else{
        puts("Hello World");
    }
}