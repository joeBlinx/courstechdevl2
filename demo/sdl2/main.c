#include <SDL2/SDL.h>
#include <stdbool.h>
#include <stdio.h>
int main(void) {
  if (SDL_Init(SDL_INIT_VIDEO)) {
    fprintf(stderr, "SDL failed to initialize: %s", SDL_GetError());
    return EXIT_FAILURE;
  }
  SDL_Window *window = SDL_CreateWindow("Tech Dev", SDL_WINDOWPOS_CENTERED,
                                        SDL_WINDOWPOS_CENTERED, 600, 400, 0);
  if (!window) {
    fprintf(stderr, "Unable to create window: %s", SDL_GetError());
    SDL_Quit();
    return EXIT_FAILURE;
  }
  SDL_Renderer *renderer =
      SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  if (!renderer) {
    fprintf(stderr, "Unable to create renderer: %s", SDL_GetError());
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_FAILURE;
  }
  bool run = true;
  int y = 100;
  while(run){
     SDL_Event event;
     SDL_WaitEvent(&event);
      switch (event.type)
      {
      case SDL_QUIT:
        run = false;
        break;
      case SDL_KEYUP:
        switch(event.key.keysym.sym){
          case SDLK_UP:
          if (y > 0){
            y -= 5;
          }
            break;
        }
      default:
        break;
      }
    SDL_SetRenderDrawColor(renderer,  125, 125, 125, 255);
    SDL_RenderClear(renderer);

    SDL_SetRenderDrawColor(renderer,  255, 0, 0, 255);
    SDL_Rect rect  = {100, y, 50, 50};
    SDL_RenderFillRect(renderer, &rect);
    SDL_RenderPresent(renderer);
  }
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}
