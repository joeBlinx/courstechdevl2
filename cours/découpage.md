 - Mécanismes de la compilation (préprocesseur, compilation, assemblage et édition de liens). 
 - Utilisation, création et compilation de bibliothèques. 
 - Compilation séparée et structuration d'un projet informatique (makefile). 
 - Gestionnaire de versions et développement collaboratif (svn, git). 
 - Documentation de code (doxygen). Analyse de couverture de code (gcov). 
 - Méthodologie de gestion des erreurs et déverminage (gdb et valgrind). 
 - Structure mémoire d'un exécutable, pile d'exécution. 
 - Approfondissement des mécanismes d'allocation statique et dynamique de mémoire, arithmétique des pointeurs, appels de fonctions
 - Portée des variables et fonctions. Compléments sur le langage C et ses bibliothèques.


Compétences à acquérir
À l'issue de cette UE, un étudiant est capable de :

- utiliser un compilateur, connaître ses options, utiliser la compilation séparée  (GCC, Clang)[compilateur](compilateur.md)
- écrire un makefile (basic)
- contribuer à un projet collaboratif (git + git workflow) [git](git.md)
- documenter un code source (Doxygen)
- déboguer un programme (gdb + valgrind + TU?)
- maîtriser la structure en mémoire d'un programme écrit en langage C (????) pointeur ?
- développer un projet de taille importante 
    - TP 



# Structure du cours
1. [Rappel de C](rappel_C.md)
    - variable/fonction/condition/loop
    - pointeur
    - prototype et header file
    - Complément de C (C99, C11)
        - initializer
        - _Generic
        - ...
2. [Compiliation](compilateur.md)
    - Rappels et info sur les compilateurs 
    - Compilation séparé
    - Makefile
3. Bibliothèque
    - Utilisation d'une bibliothèque (LDFLAGS)
    - Explication d'une bibliothèque
    - Création d'une bibliothèque (-shared, ar rcs)
4. Outils de dev
    - [git](git.md)
    - Doxygen
    - gcov et TU


# TP's