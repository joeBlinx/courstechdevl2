# Gestionnaire de version ?
scm: source code managment

C'est un outil permettant de gérer les différentes version de code source.
Version -> chaque modification du code
Il permet le partage de code source. 

Il en existe deux types:
- centralisé (svn, perforce,...)
- decenstralisé (git, mercurial)

## Centralisé
L'entiereté de l'historique de version est stocké à un seul endroit. Généralement un serveur
distant. Il est donc centralisé à un seul endroit.


### Avantages
- On peut bloquer la modification d'un fichier
- Souvent plus simple à utiliser qu'un scm décentralisé...


### Inconvénient
- un seul backup
- ...mais moins permissif
- connexion au serveur obligatoire pour utiliser le scm



## Décentralisé
Au contraire d'un scm centralisé, lorsqu'on utilise un scm décentralisé, chaque utilisateur va 
avoir une copie d'historique en local sur sa machine. De ce fait, ce genre de scm n'a pas besoin de
serveur pour fonctionner et peut tout a fonctionner en local. C'est le cas pour git.

Des services comme github ou gitlab permettent aux développeurs de se partager l'historique.

### Avantages
- X backup, X étant le nombre de developpeur
    **Mais** les backup peuvent etre désynchronisé du serveur de partage.

- Aucune connexion nécessaire pour travailler, elle n'est necessaire que pour se synchroniser avec le serveur

### Inconvénient
- la récupération du code peut être longue si on récupère tout l'historique d'un gros projet (ex: kernel Linux)


## Utilisation basique d'un SCM

La plupart si ce n'est tout les SCM fonctionne avec un système de commit qui correspond à une version de l'application.
Une utilisation classique est la suivante:
1. modification du code
2. Enregistrement d'une nouvelle version

Et sur un décentralisé
3. synchronisation du serveur

# Git 
## Qu'est ce que git ?
Git est un scm décentralisé créé en 2005 par Linus Torvald. Il est distribué sous la license GNU GPL et peut donc être utilisé
par n'importe qui. Il est également le SCM le plus populaire dans l'industrie et dans la communauté open source.
C'est un outil qui s'utilise principalement en ligne de commande, mais de nombreux GUI existe
- Tortoise Git
- Git Kraken
- Git gui
- ....

Il s'intègre également dans un grand nombre d'IDE (Integrated Development Environment):
- Vs code
- CLion
- Visual Studio
- ....

Quelques définitions:
`repository`: un dossier géré par git
`commit` : valider. Dans le cadre d'un SCM cela correspond à une version du logiciel


## Les bases de git

### Créer un repo git local

 -> TD? TP?

Deux Solutions
1. `git init` -> créer un repo local.\
Création du dossier .git et des fichiers de config necessaire
2. On crée d'abord le repo sur l'hebergeur puis on clone le repo avec `git clone URL`

La deuxième solution va plus loin que simplement créer un repository mais elle permet d'avoir sans effort le lien avec l'hebrgeur.



1. git add // ajout des fichiers pour une nouvelle version\
 -> `git add [fichiers...]`

2. git commit // création de la nouvelle version\
 -> `git commit` \
 Cela ouvre un éditeur de texte dans lequel on inscrit un message correspondant au changement apporté par cette nouvelle version 
 ```c
 double square_root(double a, double b){}
 ```
commit message: "add square_root function"\
Pour aller plus vite, `git commit -m [message]`\
Le "commit" est ajouté à la fin de l'historique\
\
**WARNING** Si c'est la première fois que vous utilisez `git commit` vous allez avoir une erreur, vous indiquant de configurer votre "username" et votre email. Pour faire cela si suffit de suivre les indications donnée par git.\
Ces deux informations seront lisible dans l'historique, cela permet de savoir à qui s'addressé en cas de problème.\
Vous pouvez parametrer ces informations en tant que `global` ou `local`:
- `global` signifie que ces informations seront utilisé pour tout les autres repo sur lesquels vous travaillerez sur votre PC sauf si une configuration local est défini
- `local` ces configurations ne sont disponible que pour ce repo 

On peut voir l'historique avec la commande
`git log`
Permet d'afficher l'historique de notre repo git.
Le commit le plus en haut est le dernier commit effectué est donc la version la plus récente.

### Synchroniser avec l'hebergeur
Pour cette partie nous considérons que nous avons utilisé la deuxième solution pour créer notre repo, la liaison avec l'hebergeur est donc deja parametré.

Pour récuperer l'historique depuis le serveur, on va utilisé la commande `git pull`. Git va alors récuperer l'historique du serveur et le fusionné avec l'historique local.

Pour "pousser" notre historique local vers le serveur, on utilise la commande `git push`. C'est l'inverse qui est fait, git fusionne l'historique local avec celui du serveur.

## Les branches

### Qu'est ce qu'une branche 

Une branche permet de créer un historique parrallèle tout en partant de la même origine. 

Lorsqu'on créée une branche, on choisit une version d'origine à partir de laquelle on va diverger. Les deux branches peuvent alors continuer d'exister en parallèle et peuvent même être fusionné l'une avec l'autre.

Dans la plupart des SCM, on utilise les branches uniquement lorsqu'on veut developper une grosse fonctionnalité sans pour autant que les changements affectent la branche principale. Par exemple: lorsqu'on veut implementer un systeme de streaming de carte dans jeux video.
Elles sont généralement très couteuse car elles sont implémenté en copiant l'intégralité des fichiers présent dans le repository.

Avec git, les branches sont très facile à crééer et très peu couteuse. Lorsqu'on utilise git, il est conseillé de créer une branche pour chaque modification que l'on veut faire. Cela permet de garder la branche principale "propre". Sous git la branche principale est généralement appelé "master", "main" ou encore "develop".

### Utilisations classiques des branches

- Création d'une branche
- Developpement d'une fonctionnalité sur cette branche
- Validation des différentes version du logiciel
- Fusion avec la branche principal
- Et on recommence

### Créer une branche.

Pour créer une branche, il suffit d'utiliser la commande\
`git branch <nom_de_branche>`.

Ensuite pour se poser sur la nouvelle branche, on utilise la commande `git switch <nom_de_branche>`, On peut également créer et passer à la nouvelle branche en une seule commande avec `git switch -c <nom_de_branche>`

### Fusionner des branches

Fusionner deux branches entre elle signifie fusionner leur historique. On peut effectuer cette opération à l'aide de la commande `git merge <branche_à_fusionner>`. Lorsqu'on utilise cette commande, git va prendre l'historique d'une branche et placer tout ce qui est nouveau à la suite de l'historique de la branche sur lequel on se trouve.
**WARNING** Lorsqu'on souhaite fusionner une branche dans une autre, il faut d'abord s'assurer que les deux branches sont bien synchronisé avec le serveur, pour ça, on executera la commande `git pull` dans les deux branches concernés.


En terme de commande git, un workflow classique sera:
- git switch -c <new_branch>
- // coding
- git add <modified_file1> <modified_file2> ...
- git commit -m "My functionnality does this"
- git pull
- git switch develop
- git pull
- git merge <new_branch>

De cette manière, nous avone developpeur une fonctionnalité dans une branch parallèle puis une fois la fonctionnalité fini nous avons basculé l'historique de cette fonctionnalité dans la branch develop





## Git avancé
### Rebase
### Rebase intéractif
### Cherry-pick



