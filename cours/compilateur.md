# Étapes de compilation
On peut diviser la compilation d'un programme écrit en language C en 3 étapes:
- Le préprocesseur
- La compilation des unités de translations
- L'assemblage et l'édition des liens

## preprocesseur
L'étape de préprocesseur consiste à remplacer tout les directices macros (celle qui commence par un #).
Les `#include <fichier>` sont remplacé par le contenu du fichier
Les `#define` sont remplacé par leur définition.
exemple:
```c
#define A puts("hello")
```
Tout les A seront remplacé par `puts("hello")`
## compilation des TU
Le compilateur va parser le fichier source, vérifier que la syntaxe utilisé correspond à ce qui est autorisé dans le standard et crée un fichier binaire correspondant au fichier source. C'est également lors cette étapes que l'on trouve les étapes d'optimisation.

## assemblage et editions des liens

Dans cette dernière étapes, le linker procède à l'édition des liens. Autrement dit, le linker va lié les utilisations de fonction/variable avec leur définition


# Utiliser un compilateur
GCC/Clang 
Pour chaque étape de la compilation, il est possible de passer une option au compilateur pour qu'il s'arrete à l'étape demandé, c'est très utile lorsque l'on veut faire de la compilation séparé. (Et on veut faire de la compilation séparé)
## Préprocesseur
L'option -E permet d'arreter la compilation juste après la gestion du préprocesseur. Cette option est rarement utile, elle sert généralement lorsque l'on veut vérifier des remplacements de macro lorsque celle cei ne compile pas.

`gcc -E main.c -o main.preproc`
## compilation des TU
L'option -c permet d'arreter la compilation après la compilation des unité de translatons. Cette option est très utilisé dans le cadre de la compilation séparé.

`gcc -xc -c main.preproc -o main.o`\
`-xc` pour que gcc interprete le fichier d'entré comme un fichier C.
## assemblage et édition des liens
L'édition des liens se fait sans ajout d'option particulière.\
`gcc main.o -o main`


## Quelques options utile
(Chiant à placer en TP, rapidement)
- `-Wall` permet d'activé tout les warnings
- `-Wextra` active tout les warnings non activé par `Wall`
- O[n] (avec n [0, 3]): niveau d'optimisation
- `pedantic` compile sans extension
- `std=..` standard C
- `-g` compile en "debug". Place des info de debug dans les fichiers binaires

## CFLAGS et LDFLAGS
# Compilation séparé
La compilation séparé est le processus qui consiste à compiler chaque fichier source séparement avant de faire l'éditions de liens avec les fichiers binaire qui ont été généré.

Avec cette méthode on peut recompiler uniquement les fichiers qui ont été modifié puis faire l'édition des liens. Plutot que de tout recompiler à chaque fois.

Prenons un projet de 3 fichiers, `main.c`, `math.c` et `gui.c`. Si on doit compiler ces fichiesr à la main on ferai\
```shell
gcc -c main.c -o main.o
gcc -c math.c -o math.o
gcc -c gui.c -o gui.o
gcc gui.o math.o main.o -o my_program
```
Facile pour 3 fichiers, mais beaucoup plus compliqué sur des gros projets qui peuvent facilement dépasser les 50-100 fichiers..

C'est pourquoi on n'utilise jamais directement le compilateur mais à la place on utilise un logiciel appelé système de compilation ou "build system". Par exemple GNU Make.

## GNU Make
GNU make est un programme qui permet la compilation séparé, il est principalement utilisé dans les programmes C ou C++. Pour utilisé GNU Make il est necessaire d'écrire un fichier nommé `Makefile`. 

Un `Makefile` est consitué d'un ensemble de règle que l'on écrit de cette façon.
```make
rule: dependances
<tab>COMMAND
```
- `rule` est un nom unique pour cette règle
- `dependances` une liste de fichiers séparé par des espaces donc la règle a besoin pour s'executer et qui peut également être une règle
- `COMMAND` est la commande à éxecuter lorsqu'on invoque la régle.

On peut ensuite executer cette règle en tapant `make <rule>` dans un terminal. Attention le makefile doit se trouver dans le dossier dans lequel on se trouve.

Exemple, si on reprend le projet de 3 fichiers
```make
build: main.c math.c gui.c
    gcc maic.c math.c gui.c -o my_program
```

On peut lancer la commande `make` sans argument, dans ce cas c'est la premiere régle qui est executé

L'un des avantages de ces règles c'est qu'elles ne s'executeront uniquement si les dependances ont été modifié.

**Exo**: Ecrire le makefile permettant de mettre en place la compilation séparé pour l'exemple de 3 fichiers


## Outils de debuggage
- valgrind
- gdb
- ASAN
- UBSAN