# Rappel 

```c
// inclusion de fichiers "headers"
#include <stdio.h> 
#include <stdlib.h>


int main(int argc, char* argv[]){ // définition de la fonction main
    if(argc > 1){ // condition
    //Si la condition est vraie
        int const number = 5; // définition d'une variable
        printf("Display a number: %d", number); //appel de la fonction printf

    }else{ 
        // Si la condition est fausese
        puts("Hello World"); // appel de la fonction puts
    } // fin de l'expression conditionnelles
} // fin de la fonction main
```
## Rappel des bases
### Les variables

Les variables peuvent être vu comme une case dans la mémoire d'un ordinateur sur laquel on aurait collé un identifiant qui est le nom que l'on donne à la variable. Et le contenu de cette case est la valeur que l'on donne à la variable.

<!-- Include image -->

Pour définir un type on suit ce shéma:\
`<type> <identifiers> = <value>;`\
Exemple: `int a = 0;`\
`type` peut etre:
- int
- double
- float
- char

À tous ces types, on peut leur ajouté:
 - `unsigned`: type non signé (aka >= 0)
 - `long`: plus grand stockage mémoire
 - `short`: plus petit stockage mémoire

`identifiers` est le nom que l'on donne à une variable, il peut être n'importe quoi sauf un de ceux la
```
auto
break
case char continue const
default do double
else extern
float for
goto
if int
long
register return
short sizeof static struct
switch
typedef
union unsigned
while
```
Ceux sont des mot clé reservé au langage.

`value` est tout simplement une valeur, compatible avec le `type`, donc on veut remplir notre variable.

### Les fonctions

On définit une fonction de cette manière
```c
int main(int argc, char* argv[])
```
Cela s'appelle le protoype de la fonction
Il est découpé en 3 parties:
- le type de retour
- l'identifiant (le nom)
- les paramètres de la fonction.

Le `type` de retour suit les mêmes restrictions que celui d'une variable, il en est de même pour l'`identifiant`

### Control flow
#### Les booléen
Un booléen est une valeur qui peut être évalué à "faux" ou "vrai". En C, tout nombre égale à 0 faux, tout le reste vaut vrai.

#### Conditional expression

- `==`: égalité
- `!=`: inégalité
- `\>`: supérieur strict
- `\>=`: supérieur ou égale
- `\<`: inférieur strict
- `\<=`: inférieur ou égale

#### if
#### for
#### while/ do while
#### switch

## Rappel de vocabulaire
- **Déclaration**: On explique au compilateur ce que represente un nom qu'on appellera un "identifier"
par exemple: `int i;` i est un entier\
*INFO* : une déclaration peut être répété de manière cohérente dans tout le programme
- **Définition**: On donne une valeur/un contenue à notre identifier\
*INFO* : Une définition doit être unique.

## Structure d'un programme C
Un programme C est constitué de:
- Déclaration de variable/type/fonction
- Définition de variable/fonction
- D'instructions
    - if
    - for
    - while
    - ...
- De Preprocesseur
    - Inclusion de bibliothèque
    - Défintion de constante


## Pointeurs
### Rappels sur la mémoire
Schéma
### Qu'est ce que c'est
C'est une type qui peut contenir l'adresse d'une variable.
on le note `type*`. type étant le type de la varaible dont on va prendre l'adresse.

On prend l'adresse d'une variable en utilisant le signe `&`.\
**Exemple :**
```c
int main(int argc, char* argv[]){
    int a = 0;
    int* p_a = &a;
    printf("%p", p_a)
}
```
Vous remarquerez qu'il y a deux pointeurs dans notre exemple. Un pointeur vers un `int` et un pointeur vers un `char[]`

Une fois qu'on a un pointeur, on peut accéder à l'élément pointé en utilisant le symbole `*`, on parle alors de deréferencement.\
**Exemple :**
```c
int main(int argc, char* argv[]){
    int a = 0;
    int* p_a = &a;
    printf("b contient l'adresse %p et ponite vers la valeur %d", p_a, *p_a);
}
```
Dela même manière on peut changer la valeur pointé en utilisant le même symbole.
```c
int main(int argc, char* argv[]){
    int a = 0;
    int* p_a = &a;
    printf("b contient l'adresse %p et ponite vers la valeur %d", p_a, *p_a);
    *p_a = 6;//on modifie la valeur pointé, valeur qui est contenu dans la variable a, a va donc être modifié
    printf("b contient l'adresse %p et ponite vers la valeur %d", p_a, *p_a); // *p_a vaut alors 6 et a également
}
```
Dans cette exemple on affiche l'adresse de a et sa valeur en passant par le pointeur. Cette exemple n'a pas beaucoup d'interet alors passons à un cas, un peu plus pratique.

Imaginons deux variables de type `int` que l'on nommera `a` et `b`. On veut faire une fonction qui échange leur valeur. Si `a` vaut `5` et `b` vaut `6`, On veut qu'une fois la fonction appelé les valeurs sont interchangé, c'est à dire que `a` vaudra `6` et `b` vaudra `5`.

```c
void swap(int a, int b){
    int tmp = a;
    a = b;
    b = tmp;
}
int main(int argv, char* argc[]){
    int a = 5;
    int b = 6;
    swap(a, b);
}
```
Cette solution ne peut pas marcher car rappelez vous, les paramètres d'une fonction sont passé par copie. Notre fonction swap ne fait donc rien ici.

La solution est de passé par pointeur. 
```c
void swap(int* p_a, int* p_b){
    int tmp = *a;
    *a = *b;
    *b = tmp;
}
int main(int argv, char* argc[]){
    int a = 5;
    int b = 6;
    swap(a, b);
}
```
Dans la fonction `swap`, `p_a` et `p_b` sont pris par copie, comme tout argument de fonction. Mais ici cela n'a pas d'importance car on copie l'adresse des variables, et non la valeur de celle ci. On peut donc manipuler les valeurs.

### chaine de caractère (ou string)