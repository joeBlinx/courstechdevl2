

int number = 5; // définition d'une variable

number = 6 // on modifie la valeur de number




5 == 4 // renvoie faux
5 != 4 // renvoie vrai
5 < 4 // renvoie faux
5 <= 4 // renvoie faux 
5 > 4 // renvoie vrai
5 >= 4 // renvoie vrai


int number = 6;
if(number){
// si number est vrai. I.e: != 0
}else{
// sinon
}

if(number >= 6){
// si number est supérieur ou égale à 6
}else{
// sinon
}



//init; condition; increment
for (int i = 0; i < 10; i += 1){
    printf("Hello %d", i);
}


int number = 5;
while (number--){
    printf("Number is %d", number);
}



int number = 0;
do{
    printf("Number is %d", number);
}while(number);



int square(int number){
    return number*number;
}


void display_hello(void);



#include <stdio.h> 
#include <stdlib.h>



// Commentaire sur ligne 

/*
 Bloc de commentaires
*/




